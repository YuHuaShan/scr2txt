@echo package start...

SET PADDLEOCR_PATH=C:\Users\leo\anaconda3\envs\paddleocr\Lib\site-packages
SET CODE_PATH=C:\workspaces\tools\scr2txt

@REM @echo package ok!!

@REM debug

@REM pyinstaller --clean -y -D --clean --exclude matplotlib -p %PADDLEOCR_PATH%\paddle\libs;%PADDLEOCR_PATH%\paddleocr;%PADDLEOCR_PATH%\paddleocr\ppocr\utils\e2e_utils;%PADDLEOCR_PATH%\paddleocr\ppstructure\table scr2txt.py -i scr2txt.ico --add-binary %PADDLEOCR_PATH%\paddle\libs;. --add-data %CODE_PATH%\scr2txt.ico;. --add-data %PADDLEOCR_PATH%\paddleocr\ppocr\utils\ppocr_keys_v1.txt;.\ppocr\utils --add-data %PADDLEOCR_PATH%\paddleocr\ppocr\utils\dict\table_structure_dict.txt;.\ppocr\utils\dict --add-data %PADDLEOCR_PATH%\layoutparser\misc\NotoSerifCJKjp-Regular.otf;.\layoutparser\misc --additional-hooks-dir=. --hidden-import extract_textpoint_slow --hidden-import tablepyxl --hidden-import tablepyxl.style

@REM release 

pyinstaller --clean -y -w -F --clean --exclude matplotlib -p %PADDLEOCR_PATH%\paddle\libs;%PADDLEOCR_PATH%\paddleocr;%PADDLEOCR_PATH%\paddleocr\ppocr\utils\e2e_utils;%PADDLEOCR_PATH%\paddleocr\ppstructure\table scr2txt.py -i scr2txt.ico --add-binary %PADDLEOCR_PATH%\paddle\libs;. --add-data %CODE_PATH%\scr2txt.ico;. --add-data %PADDLEOCR_PATH%\paddleocr\ppocr\utils\ppocr_keys_v1.txt;.\ppocr\utils --add-data %PADDLEOCR_PATH%\paddleocr\ppocr\utils\dict\table_structure_dict.txt;.\ppocr\utils\dict --add-data %PADDLEOCR_PATH%\layoutparser\misc\NotoSerifCJKjp-Regular.otf;.\layoutparser\misc --additional-hooks-dir=. --hidden-import extract_textpoint_slow --hidden-import tablepyxl --hidden-import tablepyxl.style --version-file=version.txt

@REM --exclude-module pandas 

@echo package ok!!
